import styled from "styled-components";

const ContainerContentCenterOuter = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`;
const ContainerContentCenterInner = styled.div`
    position: relative;
`;

export const SC = {ContainerContentCenterOuter, ContainerContentCenterInner}