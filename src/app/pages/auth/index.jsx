import React from "react";
import { Route, Switch, useHistory } from 'react-router-dom';
import { FormAuthorization } from "../../commponents/forms/formAuthorization";
import { FormRegistration } from "../../commponents/forms/formRegistration";
import { SC } from "./styled";

export const Auth = () => {
    let history = useHistory();
    const [loc, setLoc] = React.useState("");
    React.useEffect( () => {
        setLoc(history.location.search)
    }, [history.location.search] )
    

    return(
        <SC.ContainerContentCenterOuter> 
            <SC.ContainerContentCenterInner>
                {
                    loc !== "?registration" && <FormAuthorization />
                }
                {
                    loc === "?registration" && <FormRegistration />
                }
            </SC.ContainerContentCenterInner>
        </SC.ContainerContentCenterOuter>
    )
}