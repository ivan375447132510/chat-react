import styled from "styled-components";
import { Layout, Row } from 'antd';
import { WhiteMain, GreyLight, Offline, Online } from "../../commponents/basic/colorsThem/styled";

const LayoutMainBox = styled(Layout)`
    height: 100%;
    .ant-layout-sider, .ant-layout-header, .ant-layout-content{
        background: ${WhiteMain.light}
    }
    .ant-layout-sider{
        border-right: 1px solid rgba(${GreyLight.light50});
    }
    .ant-layout-header{
        border-bottom: 1px solid rgba(${GreyLight.light50});
    }
`;

const TitleListBox = styled(Row)`
    height: 64px;
    padding: 20px;
    border-bottom: 1px solid rgba(${GreyLight.light50});
`; 

const ContentListBox = styled(Row)`
    padding: 20px 0;
    .ant-input-group-wrapper{
        padding-left: 20px;
        padding-right: 20px;
    }
`;


//  STATUS
const StatusBox = styled.div`
    text-align: center;
    line-height: normal;
    & > *{
        margin: 0;
    }
    .ant-typography{
        margin-bottom: 0;
    }
`;

const Status = styled.div`
    line-height: normal;
    line-height: 12px;
    position: relative;
    display: inline-block;
    &::before{
        content: '';
        position: absolute;
        width: 7px;
        height: 7px;
        border-radius: 50%;
        display:inline-block;
        left: -12px;
        top: calc(50% + 1.9px);
        transform: translateY(-50%);
        background: ${({isOnline = false}) => (isOnline ? Online.light : Offline.light)};
    }
`;
// END STATUS

const ContentWrapper = styled(Layout.Content)`
    padding: 30px;
    padding-right: 0;
    display: flex;
    flex-direction: column;
`;

const ContentBox = styled.div`
    flex: 1;
    padding-right: 30px;
    padding-bottom: 10px;
    overflow: auto;
`;
const MessageInputBox = styled.div`
    margin-top: 10px;
    display: flex;
    align-items: flex-end;
    justify-content: center;
    margin-right: 30px;
`;

export const SC = {
    LayoutMainBox, 
    TitleListBox, 
    ContentListBox, 
    StatusBox, 
    Status,
    ContentWrapper,
    ContentBox,
    MessageInputBox
}