import React from "react";
import { Col, Layout, Row, Input } from 'antd';

import { SC } from "./styled";
import { UserOutlined, FormOutlined } from '@ant-design/icons';
import { Typography } from 'antd';
import { ListUsers } from "../../commponents/listUsers";
import { MessageInput } from "../../commponents/messageInput";
import { MessageItems } from "../../commponents/message";

const { Text, Title } = Typography;
const { Header, Sider, Content } = Layout;
const { Search } = Input;

const dataUsersList = [
    {id: 1, 
    name: "Эдик Тимко", 
    lastMessage: "Привет, тестовое сообщение. Привет, тестовое сообщение", 
    countLastMessage: "3",
    status: "Сейчас",
    delivered: false,
    read: false,
    isOnline: true,
    avatar: "https://sun9-24.userapi.com/impf/c845321/v845321621/fb855/9yw4_MWs2so.jpg?size=200x0&quality=96&crop=1,161,957,957&sign=34b3312150a9e1575dd89ce15f198954&ava=1"
},
{id: 2, 
    name: "Екатерина Фамилия", 
    lastMessage: "Го в Dota 2", 
    countLastMessage: false,
    status: "13:01",
    delivered: true,
    read: false,
    isOnline: false,
    avatar: "https://sun9-24.userapi.com/impf/c845321/v845321621/fb855/9yw4_MWs2so.jpg?size=200x0&quality=96&crop=1,161,957,957&sign=34b3312150a9e1575dd89ce15f198954&ava=1"
},
{id: 3, 
    name: "Марина Данилова", 
    lastMessage: "Го в Dota 2", 
    countLastMessage: false,
    status: "13:01",
    delivered: true,
    read: true,
    isOnline: false,
    avatar: "https://sun9-24.userapi.com/impf/c845321/v845321621/fb855/9yw4_MWs2so.jpg?size=200x0&quality=96&crop=1,161,957,957&sign=34b3312150a9e1575dd89ce15f198954&ava=1"
},
{id: 4, 
    name: "Евгений Васюкович", 
    lastMessage: "Го в Dota 2", 
    countLastMessage: false,
    status: "13:01",
    delivered: false,
    read: false,
    isOnline: true,
    avatar: "https://sun9-24.userapi.com/impf/c845321/v845321621/fb855/9yw4_MWs2so.jpg?size=200x0&quality=96&crop=1,161,957,957&sign=34b3312150a9e1575dd89ce15f198954&ava=1"
}
]

const MainPage = () => {

    const onSearch = (value) => {
        console.log("value search input", value);
    } 

    return(
    <SC.LayoutMainBox>
        <Sider width={330}>
            <SC.TitleListBox type="flex" justify="space-between" align="middle">
                <Col> <UserOutlined /> <Text type="secondary"> Список диалогов </Text> </Col>
                <Col> <FormOutlined /> </Col>
            </SC.TitleListBox>
            <SC.ContentListBox>
                <Search
                placeholder="Введите id пользователя"
                allowClear
                onSearch={onSearch}
                type="number"
                />
            
            {/* list users */}
            <ListUsers data={dataUsersList} />
            {/* end list users */}

            </SC.ContentListBox>
        </Sider>
        <Layout>
            <Header>
                <Row type="flex" align="middle" justify="center" style={{height: "100%"}}>
                    <SC.StatusBox>
                        <Title level={4}>Волков Иван</Title>
                        <SC.Status isOnline={true}> онлайн </SC.Status>
                    </SC.StatusBox>
                </Row>
                
            </Header>
            <SC.ContentWrapper>
                {/* content */}
                <SC.ContentBox>
                    <MessageItems isMy={false} />
                    <MessageItems isMy={true} />
                    <MessageItems isMy={false} />
                </SC.ContentBox>
                {/* end content */}
                {/* textarea message */}
                <SC.MessageInputBox>
                    <MessageInput />
                </SC.MessageInputBox>
                {/* end textarea message */}
            </SC.ContentWrapper>
        </Layout>
    </SC.LayoutMainBox>
    )
}

export default MainPage