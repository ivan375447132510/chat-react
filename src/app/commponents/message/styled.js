import { Col, Row } from "antd";
import styled from "styled-components";
import { MessageBg, WhiteMain, Black } from "../basic/colorsThem/styled";

const MessageBox = styled.div`
    margin-bottom: 15px;
    ${({isMy}) => isMy ? "margin-left: auto;" : "margin-right: auto;"}
    max-width: 700px;
    &:last-child{
        margin-bottom: 0;
    }
`;

const AvatarBox = styled(Col)`
    display: flex;
    align-items: flex-end;
    justify-content: center;
    ${({isMy}) => isMy ? "order: 1;" : "order: 0;"}
`;

const MessageText = styled(Col)`
    ${({isMy}) => isMy ? `background: ${WhiteMain.light};` : `background: ${MessageBg.light};`}
    padding: 10px;
    border-radius: 20px;
    ${({isMy}) => isMy ? "border-bottom-right-radius: 0;" : "border-bottom-left-radius: 0;"}
    box-shadow: 1px 4px 8px 0px rgba(0,0,0,.1);
    .ant-typography{
        ${({isMy}) => isMy ? `color: ${Black.light};` : `color: ${WhiteMain.light};`}
        
        margin-bottom: 0;
    }
`;

const Content = styled(Row)`
    position: relative;
    .CheckMessageBox{
        left: -25px;
        top: auto;
        bottom: -15px;
    }
`;

const DateBox = styled(Row)`
    margin-top: 10px;
    & > *{
        ${({isMy}) => isMy ? "margin-right: 8.33333333%;" : "margin-left: 8.33333333%;"}
    }
`;


export const SC = {
    MessageBox,
    AvatarBox,
    MessageText,
    Content,
    DateBox
}