import React from "react";
import moment from "moment";
import { SC } from "./styled";
import playIcon from "../../../../assets/img/play.svg"
import pauseIcon from "../../../../assets/img/pause.svg"
import waveIcon from "../../../../assets/img/wave.svg"

export const MessageAudio = ({isMy}) => {

    const [stateBtn, setStateBtn] = React.useState(false);
    const [dragonType, setDragonType] = React.useState(false);
    const [stateDuration, setStateDuration] = React.useState("00:00");
    const [progresBar, setProgresBar] = React.useState(0);
    const [progresBarClick, setProgresBarClick] = React.useState(0);
    const [progresBarBuffered, setProgresBarBuffered] = React.useState(0);
    const audioEventBoxRef = React.useRef(null);
    const audioEventRef = React.useRef(null);


    React.useEffect(() => {
        if(audioEventRef.current){
            audioEventRef.current.addEventListener("loadedmetadata", (e) => {
                let dateUpdate = moment(audioEventRef.current.duration * 1000).format("mm:ss");
                setStateDuration(dateUpdate);
                
            })
            audioEventRef.current.addEventListener("playing", (e) => {
                setStateBtn(true)
                
            })
            audioEventRef.current.addEventListener("pause", () => {
                setStateBtn(false)
            })
            audioEventRef.current.addEventListener("timeupdate", (e) => {
                const progres = (audioEventRef.current.currentTime * 100) / audioEventRef.current.duration;
                setProgresBar(progres)
                const currentTimeUpdate = audioEventRef.current.duration - audioEventRef.current.currentTime;
                let dateUpdate = moment(currentTimeUpdate * 1000).format("mm:ss");
                setStateDuration(dateUpdate)
                const bufEnd = e.srcElement.buffered.end(0);
                const bufferedData = (bufEnd * 100) / audioEventRef.current.duration;
                setProgresBarBuffered(bufferedData);
            })
            audioEventRef.current.addEventListener("ended", () => {
                setProgresBar(0);
                setStateBtn(false)
                let dateUpdate = moment(audioEventRef.current.duration * 1000).format("mm:ss");
                setStateDuration(dateUpdate)
            })
        }
    }, [])
    const controlStateAudioElement = () => {
        stateBtn ? audioEventRef.current.pause() : audioEventRef.current.play();
        
    }

    const onMouseDownFn = (e) => {
        setDragonType(true);
        console.log("onMouseDownFn")
        // setStateBtn(false)
        // audioEventRef.current.pause()
    }

    const onMouseMoveFn = (e) => {
        if(dragonType){
            if(audioEventBoxRef.current){
                const percentAudioBlock = (e.nativeEvent.layerX * 100) / audioEventBoxRef.current.offsetWidth;
                setProgresBar(percentAudioBlock);
                setProgresBarClick(percentAudioBlock);
            }
        }  
    }

    const onMouseUpFn = (e) => {
        if(audioEventBoxRef.current){
            const percentAudioBlock = (e.nativeEvent.layerX * 100) / audioEventBoxRef.current.offsetWidth;
            audioEventRef.current.currentTime = (audioEventRef.current.duration / 100) * percentAudioBlock;
            
        }
        console.log('onMouseUpFn')
        setDragonType(false);
        // setStateBtn(true)
        // audioEventRef.current.play()
    }

    return (
        <SC.AudioBox ref={audioEventBoxRef} isMy={isMy} > 
            {
                dragonType 
                ? <SC.ProgresBar width={progresBarClick} bgColor="#3a8eff" zIndex='2'></SC.ProgresBar>
                : <SC.ProgresBar width={progresBar} bgColor="#3a8eff" zIndex='2'></SC.ProgresBar>
            }
            
            <SC.ProgresBar width={progresBarBuffered} bgColor="#3175ff" ></SC.ProgresBar>
            
            <SC.Info>
            <SC.ProgresBar onMouseMove={onMouseMoveFn} onMouseDown={onMouseDownFn} onMouseUp={onMouseUpFn} style={{cursor: "pointer"}} width={progresBarBuffered} bgColor="transparent" zIndex='4' ></SC.ProgresBar>
                <SC.ButtonControl onClick={controlStateAudioElement} >
                    <img src={stateBtn ? pauseIcon : playIcon} alt="play"/>
                </SC.ButtonControl>
                <SC.WaveBox> <img src={waveIcon} alt="wave"/> </SC.WaveBox>
                <SC.DataTime> -{stateDuration} </SC.DataTime>
            </SC.Info>
            <audio ref={audioEventRef} src="https://dl3.ru-music.cc/mp3/54249.mp3"></audio>
        </SC.AudioBox>
    )
}