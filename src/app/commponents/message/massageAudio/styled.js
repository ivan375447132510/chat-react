import { SC as MessageSC } from "../styled";
import styled from "styled-components";

const AudioBox = styled(MessageSC.MessageText)`
    ${({isMy}) => isMy ? "margin-left: auto;" :  "margin-right: auto;"}
    background: #1a58ab;
    width: 14vw;
    position: relative;
    overflow: hidden;
    padding: 0.782vw;
`;

const ProgresBar = styled.div`
    width: ${({width}) => width && `${width}%` };
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
    background: ${({bgColor}) => bgColor ? `${bgColor}` : "#3a8eff" };
    z-index: ${({zIndex}) => zIndex ? `${zIndex}` : "1" };
    transition: ease 0.5s;
`;

const Info = styled.div`
    /* position: relative; */
    z-index: 10;
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: 0.7vw;
    /* padding: 0.782vw; */
    /* padding: 0.263vw; */
`;

const ButtonControl = styled.div`
    background: #0b389d;
    width: 2.084vw;
    height: 2.084vw;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    z-index: 5;
    img{
        width: 1vw;
    }
`;

const WaveBox = styled.div`
    position: relative;
    z-index: 3;
    img{width: 5.7vw;}
`;
const DataTime = styled.div`
    font-size: 1em;
    color: #fff;
    position: relative;
    z-index: 3;
`;

export const SC = {
    AudioBox,
    ProgresBar,
    Info,
    ButtonControl,
    WaveBox,
    DataTime
}