import React from "react";
import { Col, Row} from 'antd';
import PropTypes from 'prop-types';
import { SC } from "./styled";

export const MessagePhotos = ({data}) => {

    const [positionPhoto, setPositionPhoto] = React.useState({coutn: 0, before: 0, after: 0})

    React.useEffect( () => {
        let count = data.length
        if(data.length <= 4 || data.length === 8){
            setPositionPhoto({coutn: data.length -1, before: "25%", after: "25%"})
            return
        }
        if(count % 2 === 0){
            let afterData = Math.floor(count / 1.5);
            let beforeData = count - afterData;
            setPositionPhoto({coutn: beforeData -1, before: `${100 / beforeData}%`, after: `${100 / afterData}%`})
            return
        }
        if(count % 2 !== 0){ 
            let afterData = Math.ceil(count / 2);
            let beforeData = count - afterData;
            setPositionPhoto({coutn: beforeData -1, before: `${100 / beforeData}%`, after: `${100 / afterData}%`})
            return
        }
        
    }, [data])

    return(
        <Row style={{paddingTop:"10px", paddingBottom: "10px"}} gutter={[10, 10]}>
            
            {
                data?.map( (item, i) => {
                    return(
                        <Col style={{width: i <= positionPhoto.coutn ? positionPhoto.before : positionPhoto.after}} key={i}>
                            <img width={"100%"} src={item.url} alt={`photp ${i}`}/>
                        </Col>
                    )
                })
            }
        </Row>
    )
} 

MessagePhotos.propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired
};