import PropTypes from 'prop-types';
import { Col, Row, Avatar, Typography  } from "antd";
import { SC } from "./styled";
import { StateStatus } from '../stateStatus';
import { MessagePhotos } from './messagePhotos';
import { MessageAudio } from './massageAudio';

const { Paragraph, Text } = Typography;

const ava1 = "https://sun9-24.userapi.com/impf/c845321/v845321621/fb855/9yw4_MWs2so.jpg?size=200x0&quality=96&crop=1,161,957,957&sign=34b3312150a9e1575dd89ce15f198954&ava=1"

const imagesArray = [
    {
        url: "https://sun9-12.userapi.com/impf/c845321/v845321621/fb855/9yw4_MWs2so.jpg?size=960x1280&quality=96&proxy=1&sign=c25cd2ac82f50d74aa56a596b1140670&type=album"
    }, 
    {
        url: "https://sun9-12.userapi.com/impf/c845321/v845321621/fb855/9yw4_MWs2so.jpg?size=960x1280&quality=96&proxy=1&sign=c25cd2ac82f50d74aa56a596b1140670&type=album"
    }, 
    {
        url: "https://sun9-12.userapi.com/impf/c845321/v845321621/fb855/9yw4_MWs2so.jpg?size=960x1280&quality=96&proxy=1&sign=c25cd2ac82f50d74aa56a596b1140670&type=album"
    }, 
    {
        url: "https://sun9-12.userapi.com/impf/c845321/v845321621/fb855/9yw4_MWs2so.jpg?size=960x1280&quality=96&proxy=1&sign=c25cd2ac82f50d74aa56a596b1140670&type=album"
    }, 
]

export const MessageItems = ({isMy}) => {
    return(
        <SC.MessageBox isMy={isMy} >
            <SC.Content type="flex">
                {/* avatar */}
                <SC.AvatarBox span={2} isMy={isMy} > <Avatar size="large" src={ava1} /> </SC.AvatarBox>
                 {/* end avatar */}
                 {/* content */}
                <Col span={22}> 
                    <Row>
                        <SC.MessageText isMy={isMy} >
                            <Paragraph>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni libero blanditiis, dicta est nemo veniam culpa soluta beatae quibusdam sequi! Porro vitae, nam quidem nobis fuga repellendus sed atque quia. 
                            </Paragraph>
                        </SC.MessageText>
                        <Col> <MessagePhotos data={imagesArray} /> </Col>
                        <MessageAudio isMy={isMy} />
                    </Row>
                </Col>
                 {/* end content */}
                { isMy && <StateStatus meMessage={false} delivered={true} read={true} />}
                
            </SC.Content>
            {/* date */}
            <SC.DateBox isMy={isMy} justify={isMy ? "end" : "start"}>
                <Col> <Text type="secondary">Вчера в 12:30</Text> </Col>
            </SC.DateBox>
            {/* end date */}
        </SC.MessageBox>
    )
}

MessageItems.propTypes = {
    isMy: PropTypes.bool.isRequired
};