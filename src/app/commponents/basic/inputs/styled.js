import styled from "styled-components";
import { Input, Button } from 'antd';

const InputStyled = styled(Input)`
    &.ant-input-affix-wrapper{
        width: 100%;
        padding: 10px;
    }
    .ant-input-prefix{
        margin-right: 10px;
    }
`;
const ButtonStyled = styled(Button)`
    width: 100%;
    height: 47px;
`;

export const SC = {InputStyled, ButtonStyled}