import { SC } from "./styled";

export const MainContainer = ({children}) =>{
    return(
        <SC.MainContainer>
            {children}
        </SC.MainContainer>
    )
}