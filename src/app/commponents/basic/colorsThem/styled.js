//  file global colors

export const Black = {
    light: "#000",
    dark: "#fff"
}
export const Grey = {
    light: "#a29b9b",
    dark: "#fff"
}
export const GreyLight = {
    light: "#d6d2d2",
    light50: "214,210,210,.5", 
    dark: "#fff"
}

export const WhiteMain = {
    light: "#fff",
    dark: "#000"
}

export const Online = {
    light: "#4bb34b",
    dark: "#000"
}
export const Offline = {
    light: "#f74d4d",
    dark: "#000"
}

export const Danger = {
    light: "#fe786e",
    dark: "#000"
}
export const BgHoverList = {
    light: "#f4f7ff",
    dark: "#000"
}
export const SvgHover = {
    light: "#1cb7ff",
    dark: "#000"
}
export const MessageBg = {
    light: "#3175ff",
    dark: "#000"
}

