import React from "react";
import { SC } from "./styled";
import { Avatar, Spin, Col, Row } from 'antd';
import { Typography  } from 'antd';
import { StateStatus } from "../stateStatus";

const { Text, Paragraph } = Typography;

export const ListUsers = ({data}) => {
    const [isLoadedListUsers, setiSLoadedListUsers] = React.useState(false)
    

    return(
            <SC.ListBox
                style={{width: "100%"}}
                dataSource={data}
                renderItem={item => (
                    <SC.ListItem key={item.id}>
                        <SC.Visit><Paragraph type="secondary">{item.status}</Paragraph></SC.Visit>
                        <Row type="flex" style={{width: "100%"}}>
                            <Col span={4}><Avatar size="large" src={item.avatar} />
                            {item.isOnline && <SC.IsOnline />}
                            </Col>
                            <Col span={20}>
                                <Row>
                                    <Col span={24}><Text strong>{item.name}</Text></Col>
                                    <Col span={22}> <Paragraph type="secondary" ellipsis={{
                                        rows: 1,
                                        expandable: false,
                                        symbol: '',
                                    }}>
                                    {item.lastMessage}
                                    </Paragraph>
                                    </Col>
                                    <Col span={2}><StateStatus meMessage={item.countLastMessage && item.countLastMessage} delivered={item.delivered} read={item.read} /></Col>
                                </Row>
                            </Col>
                        </Row>
                    </SC.ListItem>
                )}
                >
                {isLoadedListUsers && (
                <div className="demo-loading-container">
                    <Spin />
                </div>
                )}
                </SC.ListBox>
    )
}