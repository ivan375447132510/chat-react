import styled from "styled-components";
import { List} from 'antd';
import { WhiteMain, BgHoverList, Online } from "../basic/colorsThem/styled";

const ListBox = styled(List)`
    padding: 15px 0;
    div.ant-typography, .ant-typography p{
        margin-bottom: 0;
    }
`;

const Visit = styled.div`
    position: absolute;
    right: 20px;
    top: 13px;
`;

const ListItem = styled(List.Item)`
    cursor: pointer;
    position: relative;
    padding-right: 20px;
    padding-left: 20px;
    background: ${WhiteMain.light};
    transition: 0.4s all;
    &:hover{
        background: ${BgHoverList.light};
    }
    &.active{
        background: ${BgHoverList.light};
    }
`;

const IsOnline = styled.div`
    position: absolute;
    bottom: 0px;
    right: 7px;
    border-radius: 50%;
    background: ${Online.light};
    width: 15px;
    height: 15px;
    border: 3px solid ${WhiteMain.light};
`;

export const SC = {ListBox, Visit, ListItem, IsOnline}