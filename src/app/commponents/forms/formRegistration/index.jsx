import { SC } from '../styled';
import { Form , Input, Button, Checkbox } from 'antd';
import { UserOutlined, LockOutlined, MailOutlined } from '@ant-design/icons';
import { SC as SCInput } from "../../basic/inputs/styled";
import { Form as Fform, Field } from 'react-final-form';

export const FormRegistration = () => {

    const onFinish = values => {
        console.log('Received values of form: ', values);
    };

    const onSubmit = async values => {
        window.alert(JSON.stringify(values, 0, 2))
    }

    return(
        <>
            <SC.TitleBOx>
                <SC.Title level={2}>Зарегистрироваться</SC.Title>
                <SC.SubTitle level={5}>Пожалуйста зарегистрируйте аккаунт</SC.SubTitle>
            </SC.TitleBOx>
            <SC.FormBox>
                

                {/*  */}
                {/* <Fform
      onSubmit={onSubmit}
      validate={values => {
        const errors = {}
        if (!values.username) {
          errors.username = 'Required'
        }
        if (!values.password) {
          errors.password = 'Required'
        }
        if (!values.confirm) {
          errors.confirm = 'Required'
        } else if (values.confirm !== values.password) {
          errors.confirm = 'Must match'
        }
        return errors
      }}
      render={({ handleSubmit, form, submitting, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <Field name="username">
            {({ input, meta }) => (
                <SC.FildeBox
                {...input}
            >
                <SCInput.InputStyled prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Имя" size="large" />
                {meta.error && meta.touched && <span>{meta.error}</span>}
            </SC.FildeBox>
            )}
          </Field>
          <Field name="password">
            {({ input, meta }) => (
              <div>
                <label>Password</label>
                <input {...input} type="password" placeholder="Password" />
                {meta.error && meta.touched && <span>{meta.error}</span>}
              </div>
            )}
          </Field>
          <Field name="confirm">
            {({ input, meta }) => (
              <div>
                <label>Confirm</label>
                <input {...input} type="password" placeholder="Confirm" />
                {meta.error && meta.touched && <span>{meta.error}</span>}
              </div>
            )}
          </Field>
          <div className="buttons">
            <button type="submit" disabled={submitting}>
              Submit
            </button>
            <button
              type="button"
              onClick={form.reset}
              disabled={submitting || pristine}
            >
              Reset
            </button>
          </div>
          <pre>{JSON.stringify(values, 0, 2)}</pre>
        </form>
      )}
    /> */}
                {/*  */}

                <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    // onSubmit={handleSubmit}
                    onFinish={onFinish}
                    >
                    <SC.FildeBox
                        name="name"
                        rules={[{ required: true, message: 'Please input your name!' }]}
                    >
                        <SCInput.InputStyled prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Имя" size="large" />
                    </SC.FildeBox>
                    <SC.FildeBox
                        name="email"
                        rules={[{ required: true, message: 'Please input your E-mail!' }]}
                    >
                        <SCInput.InputStyled type="email" prefix={<MailOutlined className="site-form-item-icon" />} placeholder="E-mail" size="large" />
                    </SC.FildeBox>
                    <SC.FildeBox
                        name="password"
                        rules={[{ required: true, message: 'Please input your Password!' }]}
                    >
                        <SCInput.InputStyled
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        placeholder="Пароль"
                        size="large"
                        />
                    </SC.FildeBox>
                    <SC.FildeBox
                        name="lastPassword"
                        rules={[{ required: true, message: 'Please input your Password!' }]}
                    >
                        <SCInput.InputStyled
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        placeholder="Повторите пароль"
                        size="large"
                        />
                    </SC.FildeBox>
                    <SC.FildeBox>
                        <SCInput.ButtonStyled type="primary" htmlType="submit" className="login-form-button">
                            Зарегистрироваться
                        </SCInput.ButtonStyled>
                    </SC.FildeBox>
                </Form>
                    <div> <SC.LinkForm to="/auth/?authorization"> Авторизоваться </SC.LinkForm> </div>
            </SC.FormBox>
        </>
    )
}