import { SC } from '../styled';
import { Form, Input, Button, Checkbox } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { SC as SCInput } from "../../basic/inputs/styled";

export const FormAuthorization = () => {

    const onFinish = values => {
        console.log('Received values of form: ', values);
    };

    return(
        <>
            <SC.TitleBOx>
                <SC.Title level={2}>Войти в аккаунт</SC.Title>
                <SC.SubTitle level={5}>Пожалуйста войдите в свой аккаунт</SC.SubTitle>
            </SC.TitleBOx>
            <SC.FormBox>
                <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    >
                    <SC.FildeBox
                        name="username"
                        rules={[{ required: true, message: 'Please input your Username!' }]}
                    >
                        <SCInput.InputStyled prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Логин" size="large" />
                    </SC.FildeBox>
                    <SC.FildeBox
                        name="password"
                        rules={[{ required: true, message: 'Please input your Password!' }]}
                    >
                        <SCInput.InputStyled
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        placeholder="Пароль"
                        size="large"
                        />
                    </SC.FildeBox>
                    <SC.FildeBox>
                        <SCInput.ButtonStyled type="primary" htmlType="submit" className="login-form-button">
                            Войти в аккаунт
                        </SCInput.ButtonStyled>
                    </SC.FildeBox>
                </Form>
                    <div> <SC.LinkForm to="/auth/?registration"> Зарегистрироваться </SC.LinkForm> </div>
            </SC.FormBox>
        </>
    )
}