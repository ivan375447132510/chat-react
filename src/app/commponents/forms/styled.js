import styled from "styled-components";
import { Typography, Form } from 'antd';
import { Black, Grey, GreyLight } from "../basic/colorsThem/styled";
import { Link } from "react-router-dom";



const Title = styled(Typography.Title)`
    &.ant-typography{
        color: ${Black.light};
    }
`;
const SubTitle = styled(Typography.Title)`
    &.ant-typography{
        color: ${Grey.light};
    }
`;

const TitleBOx = styled.div`
    margin-bottom: 30px;
    text-align: center;
    ${Title}, ${SubTitle}{
        &.ant-typography{
            margin: 0;
            margin-bottom: 5px;
            &:last-child{margin-bottom: 0;}
        }
    }
`;

const FormBox = styled.div`
    border-radius: 10px;
    padding: 30px;
    box-shadow: 0 0 10px ${GreyLight.light};
    text-align: center;
    width: 390px;
`;

const FildeBox = styled(Form.Item)`
    margin-bottom: 25px;
`;

const LinkForm = styled(Link)`
    color: ${Grey.light};
`;

export const SC = {TitleBOx, Title, SubTitle, FormBox, FildeBox, LinkForm}