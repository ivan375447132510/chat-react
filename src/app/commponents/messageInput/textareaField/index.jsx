import React from "react";
import { Input } from 'antd';


const { TextArea } = Input;
export const TextareaField = ({text, updateMessage}) => {


    const hendleOnChange = (e) => {
        let start  = e.target.selectionStart;
        let end  = e.target.selectionEnd;
        let text = e.target.value;
        updateMessage(text, e);
    }
    const hendleOnClick = (e) => {
        // const start  = e.target.value + "1";
        const start  = e.target.selectionStart;
        const end  = e.target.selectionEnd;
        const value  = e.target.value.length;
        // updateMessage(start);
        console.log('start', start);
        console.log('end', end);
        console.log('value', end);
    }
    return (
        <TextArea autoSize={{ minRows: 1, maxRows: 4 }} value={text} onChange={hendleOnChange} placeholder="Введите текст сообщения..." /> 
    )
}