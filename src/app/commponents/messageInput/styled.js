import styled from "styled-components";
import {SvgHover} from "../basic/colorsThem/styled";

const InputBox = styled.div`
    width: 100%;
    max-width: 1000px;
    position: relative;
    textarea{
        resize: none;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 45px;
        padding-right: 82px;
    }
`;

const IconsBox = styled.div`
    position: absolute;
    top: 50%;
    ${({position}) => position && `${position} : 0;`}
    transform: translateY(-50%);
    z-index: 1;
    display: flex;
    padding: 0 15px;
    & > *{
        cursor: pointer;
        margin-left: 10px;
        font-size: 19px;
        &:first-child{
            margin-left: 0;
        }
    }
    .anticon{
        svg path{
            transition: 0.4s all;
        }
        &:hover{
        svg path{
        fill: ${SvgHover.light};
        transition: 0.4s all;
        transform: scale(0.95);
        }
    }
    }
    
`;

export const SC = {InputBox, IconsBox}