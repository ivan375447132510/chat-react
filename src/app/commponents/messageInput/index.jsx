import React from "react";
import { SC } from "./styled";
import { SendOutlined, AudioOutlined, CameraOutlined } from '@ant-design/icons';
import { EmojiComponent } from './emoji';
import { TextareaField } from './textareaField';

export const MessageInput = () => {
    const [textareaText, setTextareaText] = React.useState('');
    const [eventTextarea, setEventTextarea] = React.useState(null);
    const [position, setPosition] = React.useState();

    const updateMessage = (text, event) => {
        setTextareaText(text);
        setEventTextarea(event);
    }

    const updateMessageEmoj = (emoji) => {
        if(eventTextarea === null){
            setTextareaText(emoji) 
        }else{
            const eventStart  = eventTextarea.target.selectionStart;
            const eventEnd  = eventTextarea.target.selectionEnd;
            const finText = eventTextarea.target.value.substring(0, eventStart) + emoji + eventTextarea.target.value.substring(eventEnd);
            setTextareaText(finText);
            setPosition(eventStart + 2)
        }
        
        
        
    }
    
    React.useEffect(() => {
        if (position && eventTextarea) {
            eventTextarea.target.focus();
            eventTextarea.target.setSelectionRange(position, position);
            setPosition();
        }
    }, [textareaText, position, eventTextarea])

    return (
        <SC.InputBox> 
            <SC.IconsBox position="left">
                <EmojiComponent updateMessage={updateMessageEmoj} />
            </SC.IconsBox>
            <TextareaField text={textareaText} updateMessage={updateMessage} />
            <SC.IconsBox position="right">
                <CameraOutlined />
                <AudioOutlined />
                <SendOutlined />
            </SC.IconsBox>
        </SC.InputBox>
    )
}