import React from "react";
import Picker, { SKIN_TONE_MEDIUM_DARK } from 'emoji-picker-react';
import { SmileOutlined } from '@ant-design/icons';
import { SC } from "./styled";

export const EmojiComponent = ({updateMessage}) => {

    const emojiRef = React.useRef(null);
    const [isShowEmojiBlock, setSsShowEmojiBlock] = React.useState(false);
    // const [chosenEmoji, setChosenEmoji] = React.useState(null);

    const hendleOutsideClick = e => {
        if(!e.path.includes(emojiRef.current)){
            setSsShowEmojiBlock(false)
        }
    }

    const hendleClick = () => {
        setSsShowEmojiBlock(!isShowEmojiBlock);
    }

    React.useEffect(() => {
        document.body.addEventListener('click',  hendleOutsideClick)
    }, [])

    const onEmojiClick = (event, emojiObject) => {
        // setChosenEmoji(emojiObject);
        updateMessage(emojiObject.emoji)
        console.log(emojiObject);
    }
   

    return(
        <SC.EmojiBox ref={emojiRef} >
            { isShowEmojiBlock && 
            <SC.EmojiBlockBox>
            <Picker onEmojiClick={onEmojiClick} disableAutoFocus={true} skinTone={SKIN_TONE_MEDIUM_DARK} groupNames={{smileys_people:"PEOPLE"}}/>
            {/* { chosenEmoji && <EmojiData chosenEmoji={chosenEmoji}/>} */}
            </SC.EmojiBlockBox>
            }
            <SmileOutlined onClick={hendleClick} />
        </SC.EmojiBox>
    )
}