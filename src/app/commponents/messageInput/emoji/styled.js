import styled from "styled-components";


const EmojiBox = styled.div`
    display: inline-block;
`;

const EmojiBlockBox = styled.div`
    position: absolute;
    bottom: 150%;
    left: 0;
`;

export const SC = {EmojiBox, EmojiBlockBox}