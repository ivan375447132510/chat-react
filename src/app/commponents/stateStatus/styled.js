import styled from "styled-components";
import { Danger, WhiteMain, SvgHover } from "../basic/colorsThem/styled"


const CheckMessage = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`;

const StateStatusBox = styled.div`
    width: 15px;
    height: 15px;
    border-radius: 50%;
    background: ${Danger.light};
    font-size: 10px;
    display: flex;
    align-items: center;
    justify-content: center;
    color: ${WhiteMain.light};
    font-weight: 900;
`;


const CheckMessageInner = styled.div`
    position: relative;
    svg path{fill: ${SvgHover.light};}
    .anticon-check:nth-of-type(2){
        position: absolute;
        top: 5px;
        left: -4px;
    }
`;


export const SC = {
    StateStatusBox,
    CheckMessage,
    CheckMessageInner
}

