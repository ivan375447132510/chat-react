import { SC } from "./styled";
import { CheckOutlined } from '@ant-design/icons';


export const StateStatus = ({meMessage, delivered, read}) => {
    return(
        <SC.CheckMessage className="CheckMessageBox">  
        {meMessage && <SC.StateStatusBox>{meMessage}</SC.StateStatusBox>} 
        {
            delivered || read
            ? 
            <SC.CheckMessageInner>
            { delivered && <CheckOutlined />} 
            { read && <CheckOutlined /> }
            </SC.CheckMessageInner>
            
        : '' 
        }
            
        
        </SC.CheckMessage>
    )
}