import { Route, Switch} from 'react-router-dom';
import { Auth } from '../../app/pages/auth';
import MainPage from '../../app/pages/main';

export const Routs = () => {
    return(
        <Switch>
            <Route path='/auth' render={() => <Auth /> } /> 
            <Route path='/mi' render={() => <MainPage />} />
            {/* <Route path='/profile/:userId?' render={<div> не главная </div>} /> */}
            {/* <Route path='/dialogs' render={withSuspense(DialogsContainer)} />
            <Route path='/users' render={() => <UsersContainer />} />
            <Route path='/news' render={() => <News />} />
            <Route path="/login" render={() => <LoginPage />} />
            <Route path="*" render={() => <div>404</div>} /> */}
        </Switch>
    )
}