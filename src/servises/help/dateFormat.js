import moment from "moment";

export const dataFormat = (value, sec = false) => {
    
    if(!sec){
        return getFormst(value);
    }
    if(sec){
        const newValue = value - sec;
        return getFormst(newValue);
    }

}

export const getFormst = (value) => {
    const seconds = moment.duration(value, 'seconds').seconds();
    const minutes = moment.duration(value, 'seconds').minutes();
    const newMinutes = dataTest(minutes);
    const newSeconds = dataTest(seconds);
    return "-" + newMinutes + ':' + newSeconds;
}

export const dataTest = (value) => {
    if(value <= 9){
        value =  0 + '' + value;
    }
    return value;
}