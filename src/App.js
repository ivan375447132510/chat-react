import { MainContainer } from "./app/commponents/basic/mainContainer";
import { Auth } from "./app/pages/auth";
import { Routs } from "./servises/routs";


function App() {
  return (
      <MainContainer>
        <Routs />
      </MainContainer>
  );
}

export default App;
